@include('inc.header')

<div class="container">
        <legend >Read Article</legend>
        <div class="line"></div>
        <p class="display-4">{{ $articles->title }}</p>
        <p class="lead">{{ $articles->description }}</p>
        <div class="line"></div>
        <a href="{{ url('/') }}" class="btn btn-primary">Back</a>
</div>

@include('inc.footer')